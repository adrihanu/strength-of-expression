# Strength of expression

System for assessing the strength of expression for selected phrases coming from natural language.

The aim of the study was to show the differences in the assessment of sentiment between survey research and statistical methods of data analysis. In the work, a corpus of several emotionally marked expressions in English was prepared and then their numerical value was determined using the extended Saaty scale. The statistical method of sentiment analysis (Naive Bayes), based on a set of data from a selected website (Twitter), was used to evaluate the tested expressions. Then the obtained results were compared with the survey research. In order to use and test the algorithm, a simple web application written in Python using the Dash framework has been prepared.

Tags: `Python`, `Flask`, `Dash`, `R Markdown`, `Natural language processing`, `NLP`, `Saaty's scale`, `Naive Bayes classifier`, `Machine learning`

## Running

1. Install `docker` and `docker-compose` on your system,
2. Make sure `dockerd` is running.
3. Run `docker-compose up` inside main directory with this project.
4. Go to `http://0.0.0.0:8050/` in your browser.

## Creating classifier

Run `docker-compose exec app pipenv run python init.py` to generate new classifier.

For current model accuracy is `0.996` and most informative features are:

```text
       :(      Negati : Positi =   2068.0 : 1.0
       :)      Positi : Negati =   1634.7 : 1.0
 follower      Positi : Negati =     37.4 : 1.0
   arrive      Positi : Negati =     32.1 : 1.0
      bam      Positi : Negati =     24.2 : 1.0
      sad      Negati : Positi =     19.8 : 1.0
community      Positi : Negati =     15.6 : 1.0
     blog      Positi : Negati =     14.2 : 1.0
     sick      Negati : Positi =     13.1 : 1.0
  awesome      Positi : Negati =     12.5 : 1.0
```

### Process

#### Data preparation

Example:

```python
tweet = "@person for being top engaged member:)"
```


#### Tokenizing the data

Splitting strings into smaller parts.

Example:

```python
tokenized_tweet = ['@person', 'for', 'being', 'top', 'engaged', 'member', ':)']
```

#### Normalizing

Converting a word to its canonical form.

I used lemmatization(analyzes the the word and its context to convert it to a normalized form) 
not stemming(only removes affixes from a word)

Example:

```python
normalized_tweet = ['@person', 'for', 'be', 'top', 'engage', 'member', ':)']
```

### Removing noise from the data

I removed hyperlinks, @tags, punctuation and special characters.

Example:

```python
removed_noise_tweet = ['top', 'engage', 'member', ':)']
```

### Preparing data for the model

Model requires a dictionary with words as keys and True as values
