# Load classifier
import json
import pickle
from nltk import NaiveBayesClassifier, MaxentClassifier
from tokenizer import *

f = open('naiveBayesClassifier.pickle', 'rb')
naiveBayesClassifier = pickle.load(f)
f.close()

f = open('maxentClassifier.pickle', 'rb')
maxentClassifier = pickle.load(f)
f.close()


# Classifier function
def classify(text, classifier):
    sentiment = classifier.classify(get_tokens(text))

    tokens = get_tokens(text)
    dist = classifier.prob_classify(tokens)

    #Values for text
    text_values = {}
    for label in dist.samples():
        text_values[label] = "{:.4f}%".format(dist.prob(label))

    #Values for tokens
    tokens_values = {}
    for token in tokens.keys():
        token_values = {}
        dist = classifier.prob_classify({token: True})
        for label in dist.samples():
            token_values[label] = "{:.4f}%".format(dist.prob(label))
        tokens_values[token] = token_values

    tokens_values = json.dumps(tokens_values, indent=2).replace('\n', '\n\n')
    return sentiment, text_values, tokens_values

if __name__=="__main__":
    print(naiveBayesClassifier.show_most_informative_features(100))


