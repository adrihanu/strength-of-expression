import nltk
from tokenizer import *
#### Prepering data
nltk.download('punkt')
nltk.download('twitter_samples'); # Downolad sample tweets

from nltk.corpus import twitter_samples # Import samples

positive_tweets = twitter_samples.strings('positive_tweets.json') # 5000 positive tweets
negative_tweets = twitter_samples.strings('negative_tweets.json') # 5000 negative tweets
text = twitter_samples.strings('tweets.20150430-223406.json') # 20000 tweets with no sentiments



##### Tokenizing the data (splitting strings into smaller parts)
#### Normalizing the data (converting a word to its canonical form)
# 1. Stemming - removes affixes from a word
# 2. Lemmatization - analyzes the the word and its context to convert it to a normalized form
#### Removing noise from the data(Hyperlinks, @tags, punctuation and special characters)


positive_cleaned_tokens_list = []
negative_cleaned_tokens_list = []

for text in twitter_samples.strings('positive_tweets.json'):
    positive_cleaned_tokens_list.append(get_tokens(text))

print('\nNEG')
for text in twitter_samples.strings('negative_tweets.json'):
    negative_cleaned_tokens_list.append(get_tokens(text))


#### Determining word density

def get_all_words(cleaned_tokens_list):
    for tokens in cleaned_tokens_list:
        for token in tokens:
            yield token

all_pos_words = get_all_words(positive_cleaned_tokens_list)

from nltk import FreqDist

freq_dist_pos = FreqDist(all_pos_words)

#### Preparing Data for the model

# model requires a dictionary with words as keys and True as values
def get_data_for_model(cleaned_tokens_list):
    for tokens in cleaned_tokens_list:
        yield dict([token, True] for token in tokens)

positive_tokens_for_model = get_data_for_model(positive_cleaned_tokens_list)
negative_tokens_for_model = get_data_for_model(negative_cleaned_tokens_list)


# Splitting the dataset for training and testing the model
# attaches a Positive or Negative label to each text
import random
positive_dataset = [(token_dict, "Positive")
                     for token_dict in positive_tokens_for_model]

negative_dataset = [(token_dict, "Negative")
                     for token_dict in negative_tokens_for_model]

dataset = positive_dataset + negative_dataset

random.shuffle(dataset)

train_data = dataset[:8500]
test_data = dataset[8500:]



#### Building, testing and saving the models
import pickle
from nltk import classify
from nltk import NaiveBayesClassifier, MaxentClassifier, DecisionTreeClassifier
'''
# DecisionTreeClassifier
decisionTreeClassifier = DecisionTreeClassifier.train(train_data, depth_cutoff=1, support_cutoff=1, verbose=1)
f = open('decisionTreeClassifier.pickle', 'wb')
pickle.dump(decisionTreeClassifier, f)
f.close()
'''
# NaiveBayesClassifier
naiveBayesClassifier = NaiveBayesClassifier.train(train_data)
f = open('naiveBayesClassifier.pickle', 'wb')
pickle.dump(naiveBayesClassifier, f)
f.close()
print("NB accuracy is:", classify.accuracy(naiveBayesClassifier, test_data))
print(naiveBayesClassifier.show_most_informative_features(10))

# MaxentClassifier 
maxentClassifier = MaxentClassifier.train(train_data, max_iter=100)
f = open('maxentClassifier.pickle', 'wb')
pickle.dump(maxentClassifier, f)
f.close()
print("ME accuracy is:", classify.accuracy(maxentClassifier, test_data))
print(maxentClassifier.show_most_informative_features(10))



