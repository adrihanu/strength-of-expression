import nltk

if __name__ == "__main__": pass
else:
    nltk.download('stopwords')
    nltk.download('wordnet') # Lexical database for the English language
    nltk.download('averaged_perceptron_tagger') # Used to determine the context of a word in a sentence

from nltk.tag import pos_tag # tagging algorithm - noun, verb etc.
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.tokenize import word_tokenize

import re, string
from nltk.corpus import stopwords
stop_words = stopwords.words('english')
stop_words.append(':(')
stop_words.append('(:')
stop_words.append(':)')
stop_words.remove('not')
#stop_words.remove('can')
#stop_words.remove('will')
stop_words.remove('do')
import contractions
from nltk.tokenize.casual import TweetTokenizer

# Get tokens
def get_tokens(text):
    text = contractions.fix(text)
    #print('####################################')
    #custom_tokens = remove_noise(word_tokenize(text), stop_words)
    #print(custom_tokens)
    custom_tokens = remove_noise(TweetTokenizer().tokenize(text), stop_words)

    if "not like" in text:
        print(text)
        print(custom_tokens)
    return dict([token, True] for token in custom_tokens)


def remove_noise(tokens, stop_words = ()):

    cleaned_tokens = []

    for token, tag in pos_tag(tokens):
        token = re.sub('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+#]|[!*\(\),]|'\
                       '(?:%[0-9a-fA-F][0-9a-fA-F]))+','', token)
        token = re.sub("(@[A-Za-z0-9_]+)","", token)

        if tag.startswith("NN"):
            pos = 'n'
        elif tag.startswith('VB'):
            pos = 'v'
        else:
            pos = 'a'

        lemmatizer = WordNetLemmatizer()
        token = lemmatizer.lemmatize(token, pos)

        if len(token) > 0 and token not in string.punctuation and token.lower() not in stop_words:
            cleaned_tokens.append(token.lower())
    cleaned_tokens = merge_contractions(cleaned_tokens);
    return cleaned_tokens

def merge_contractions(tokens):
    to_remove = []
    for i, token in enumerate(tokens):
        previous_token = tokens[i-1]

        if (i-2 < 0): previous_token_2 = ''
        else: previous_token_2 = tokens[i-2]

        if i+1 < len(tokens): next_token = tokens[i+1]
        else: next_token = ""

        to_remove.append(i+1)
        if previous_token == 'not':
            tokens[i] = previous_token_2+ " not "+token
    tokens = [x for x in tokens if not x=="not"]
    return tokens;

if __name__ == "__main__":
    sample = "I don't like this guy. He can't even work :) :(."
    print(get_tokens(sample));

