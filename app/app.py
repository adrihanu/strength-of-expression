import json
import dash
import dash_html_components as html
import dash_core_components as dcc
from helper import *
from dash.dependencies import Input, Output
app = dash.Dash(__name__)
app.config.suppress_callback_exceptions = True

app.layout = html.Div([
    dcc.Tabs(id="tabs-styled-with-inline", value='adrian-tab', children=[
        dcc.Tab(label='Adrian', value='adrian-tab', selected_style={'color': 'white', 'backgroundColor': '#119DFF'}),
    ]),
    html.Div(id='tabs-content-inline')
])


@app.callback(Output('tabs-content-inline', 'children'),
              [Input('tabs-styled-with-inline', 'value')])
def render_content(tab):
    if tab == 'adrian-tab':
        return html.Div([
            html.Div(dcc.Input(id='adrian-input', type='text', placeholder="Text to analyze",  style={'padding':'20px', 'float':'left'})),
            html.Button('Analyze', id='adrian-button',style={'padding':'20px'}),
            html.Div(id='adrian-result', children='')
        ])

#ADRIAN
@app.callback(
    dash.dependencies.Output('adrian-result', 'children'),
    [dash.dependencies.Input('adrian-button', 'n_clicks')],
    [dash.dependencies.State('adrian-input', 'value')])
def update_adrian_output(clicks, text):
    if not text: return '{}. No text provided'.format(clicks)
    tokens = get_tokens(text)
    nb = classify(text, naiveBayesClassifier);
    me = classify(text, maxentClassifier);
    return dcc.Markdown('''
**Text**: *{}*
#### Tokens: {}

## Naive Bayes
**Sentiment:** {}

**Values:** {}

**Token values:** {}

## MaxEnt
**Sentiment:** {}

**Values:** {}

**Token values:** {}
'''.format(text, list(tokens), nb[0], nb[1], nb[2], me[0], me[1], me[2] ))




if __name__ == '__main__':
    app.run_server(host="0.0.0.0", debug=True, dev_tools_hot_reload=True)
